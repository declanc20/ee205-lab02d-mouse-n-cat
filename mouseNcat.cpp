///////////////////////////////////////////////////////////////////////////////
//           University of Hawaii, College of Engineering
/// @brief   Lab02d - Mouse `n Cat - EE 205 - Spr 2022
///
/// This C++ program will either:
///   1.  Print the command line arguments in reverse order
///   2.  or, if there are no command line arguments, print all of the
///       environment variables passed into the program
///
/// @file    mouseNcat.cpp
/// @version 1.0 - Initial version
///
/// Compile: $ g++ -o mouseNcat mouseNcat.cpp
///
/// Usage:  mouseNcat [param1] [param2] ...
///
/// Example:
///   With a command line parameter:
///   $ ./mouseNcat I am Sam
///   Sam
///   am
///   I
///   $
///
///   Without a command line parameter:
///   $ ./mouseNcat
///   SHELL=/bin/bash
///   ...
///   SSH_TTY=/dev/pts/0
///
/// @author   declan campbell <declanc@hawaii.edu>
/// @date     1-22-22
///////////////////////////////////////////////////////////////////////////////


#include <iostream>
#include <cstdlib>

// Note the new main() parameter:  envp
int main( int argc, char* argv[], char* envp[] ) {
  
   
//debug to make sure right number of charactrers read in std::cout << argc << std::endl;

   if (argc > 1){ //print out in reverse order if given parameters 
for (int i = argc-1 ; i>0 ; i--) {   //start at the last string (have to -1 because null)
    std::cout << argv[i] << std::endl; //print out the string in that spot in the array
      }
 }

else {
 for (int i = 0 ; envp[i] != NULL ; i++) {   //else, not given paramters then print environment variables 
    std::cout << envp[i] << std::endl; //print out env variables
   
       }
   }

   std::exit( EXIT_SUCCESS );  //exit status successful
   return 0; //exit program with status of zero

   }
